import * as types from '../constants/ActionsTypes';
import * as apis from '../constants/ApiConstants';
import {callApi} from '../utils/ApiUtils';


export const fetchUserStart = () => ({
    type: types.FETCH_USER_START
});

export const fetchUserSuccess = user => ({
    type: types.FETCH_USER_SUCCESS,
    user
})

export const fetchUserFail = () => ({
    type: types.FETCH_USER_FAIL
});

export const fetchUser = userId => async (dispatch) => {
    dispatch(fetchUserStart());

    const { json } = await callApi(apis.USER_PROFILE.replace(':userId', userId));

    dispatch(fetchUserSuccess(json));
}

export default fetchUser;