import * as types from '../constants/ActionsTypes';


const initialState = {
    user: null,
    fetching: false
};

const user = (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_USER_START:
            return {
                ...state,
                fetching: true
            };
        case types.FETCH_USER_SUCCESS:
            return {
                ...state,
                fetching: true,
                user: action.user
            };
        case types.FETCH_USER_FAIL:
            return {
                ...state,
                fetching: false
            };
        default:
            return state;
    }
}

export default user;