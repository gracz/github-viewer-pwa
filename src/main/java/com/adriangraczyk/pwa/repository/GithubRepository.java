package com.adriangraczyk.pwa.repository;

import com.adriangraczyk.pwa.model.domain.GitHubUser;
import com.adriangraczyk.pwa.model.domain.Repository;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collection;

import static com.adriangraczyk.pwa.repository.GithubRepository.GITHUB_LINK;

@FeignClient(name = "GithubRepository", url = GITHUB_LINK)
public interface GithubRepository {

    String GITHUB_LINK = "https://api.github.com/users";

    @GetMapping("/{name}")
    GitHubUser getGithubUser(@PathVariable("name") String name);

    @GetMapping("/{name}/repos")
    Collection<Repository> getRepositories(@PathVariable("name") String name);
}
