package com.adriangraczyk.pwa.controller;

import com.adriangraczyk.pwa.service.GithubService;
import com.adriangraczyk.pwa.model.api.GithubUserResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class GitHubController {

    private final GithubService githubService;

    @GetMapping("/{userName}")
    public GithubUserResponse retrieveGitHubUser(@PathVariable String userName,
                                                 @RequestParam(name = "includeForks", required = false) boolean includeForks) {
        return githubService.retrieveGitHubUser(userName, includeForks);
    }
}
