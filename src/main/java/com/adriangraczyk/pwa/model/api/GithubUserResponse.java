package com.adriangraczyk.pwa.model.api;

import com.adriangraczyk.pwa.model.domain.GitHubUser;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class GithubUserResponse {

    private GitHubUser githubUser;
    private Collection repositories;
}
