import 'babel-polyfill';
import 'isomorphic-fetch';
import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';

import OfflinePluginRuntime from 'offline-plugin/runtime';

import './index.scss';
import App from './containers/App';

OfflinePluginRuntime.install();

render(
    <Provider store={configureStore()}>
        <App />
    </Provider>,
    document.getElementById('root')
);

