package com.adriangraczyk.pwa.service;


import com.adriangraczyk.pwa.model.api.GithubUserResponse;
import com.adriangraczyk.pwa.model.domain.Repository;
import com.adriangraczyk.pwa.repository.GithubRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class GithubService {
    private final GithubRepository githubRepository;

    public GithubUserResponse retrieveGitHubUser(String userName, boolean includeForks) {
        return GithubUserResponse.builder()
                .githubUser(githubRepository.getGithubUser(userName))
                .repositories(retrieveRepositories(userName, includeForks))
                .build();
    }

    private List<Repository> retrieveRepositories(String userName, boolean includeForks) {
        return githubRepository.getRepositories(userName)
                .stream()
                .filter(includeForks(includeForks))
                .collect(toList());
    }


    private Predicate<Repository> includeForks(boolean includeForks) {
        return r -> includeForks || !r.isFork();
    }
}
