package com.adriangraczyk.pwa.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GitHubUser {

    private String login;
    private String id;
    private String name;

    @JsonProperty("avatar_url")
    private String avatarUrl;

    private String url;
}
