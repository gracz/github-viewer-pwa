import React, {Component} from 'react';
import {connect} from 'react-redux';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import {Card, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import {orange500, lightWhite, fullWhite, lightBlue500} from 'material-ui/styles/colors';
import fetchUser from '../actions/UserActions'
import debounce from 'debounce';


const styles = {
    cardStyle: {
        display: 'block',
        transitionDuration: '0.3s',
        padding: '5vw'
    },
    errorStyle: {
        color: orange500,
    },
    underlineStyle: {
        borderColor: lightWhite,
    },
    lightWhite: {
        color: lightWhite,
    },
    fullWhite: {
        color: fullWhite,
    },
    appBar: {
        backgroundColor: lightBlue500,
    }
};

const focusSearchInputText = input => {
    input.focus();
};

class App extends Component {

    constructor(props) {
        super(props);
        this.fetchUserProfile = debounce(this.fetchUserProfile, 500);
        this.state = {
            userName: null
        }
    }

    render() {
        const {user, fetching} = this.props.user;
        console.log(user + '    |    ' + fetching);
        console.log('user props: ' + this.props.user);
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                <div>
                    <AppBar style={styles.appBar}
                            iconElementLeft={<TextField floatingLabelText="Github username"
                                                        inputStyle={styles.lightWhite}
                                                        floatingLabelStyle={styles.lightWhite}
                                                        floatingLabelFocusStyle={styles.fullWhite}
                                                        floatingLabelShrinkStyle={styles.fullWhite}
                                                        underlineFocusStyle={styles.underlineStyle}
                                                        onChange={this.onTextFieldChange.bind(this)}
                                                        ref={focusSearchInputText}/>}/>

                    {user && <Card style={styles.cardStyle}>
                        <CardHeader title={user.githubUser.name} subtitle={user.githubUser.id}
                                    avatar={user.githubUser.avatarUrl}/>
                        <CardTitle title="Repositories" subtitle="with forks"/>
                        <CardText expandable={true}>
                            {user.repositories && user.repositories.forEach((value) => {
                                value.full_name
                            })}
                        </CardText>
                    </Card>}
                </div>
            </MuiThemeProvider>
        );
    }

    onTextFieldChange(e) {
        this.state.userName = e.target.value;
        this.fetchUserProfile();
    }

    fetchUserProfile() {
        const {userName} = this.state;
        if (userName) {
            this.props.fetchUser(this.state.userName);
        }
    }
}

const mapStateToProps = (state) => {
    const {user} = state;
    return {
        user
    };
}

export default connect(mapStateToProps, {fetchUser})(App);