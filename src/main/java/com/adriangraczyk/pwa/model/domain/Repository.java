package com.adriangraczyk.pwa.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Repository {

    @JsonProperty("full_name")
    private String fullName;

    private String description;

    @JsonProperty("clone_url")
    private String cloneUrl;

    private boolean isFork;

    private LocalDateTime updatedAt;

    private String name;
}
